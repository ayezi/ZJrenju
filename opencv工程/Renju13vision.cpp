#include "include.h"
#include <conio.h>
using namespace std;
using namespace cv;


c_renju_t c_renju;


HBITMAP	hBmp;
HBITMAP	hOld;
HDC hScreen;
HDC	hCompDC;
void Screen() {

	static uint8_t init_flag = 0;
	//创建画板
	hScreen = GetDC(GetDesktopWindow());
	hCompDC = CreateCompatibleDC(hScreen);
	//取屏幕宽度和高度
	int		nWidth = GetSystemMetrics(SM_CXSCREEN);
	int		nHeight = GetSystemMetrics(SM_CYSCREEN);
	//创建Bitmap对象
	if (init_flag == 0)
	{
		hBmp = CreateCompatibleBitmap(hScreen, nWidth, nHeight);
		init_flag = 1;
	}
	hOld = (HBITMAP)SelectObject(hCompDC, hBmp);
	BitBlt(hCompDC, 0, 0, nWidth, nHeight, hScreen, 0, 0, SRCCOPY);
	SelectObject(hCompDC, hOld);
	//释放对象
	ReleaseDC(GetDesktopWindow(), hScreen);
	DeleteDC(hScreen);
	DeleteDC(hCompDC);
}

//把HBITMAP型转成Mat型
Mat v_mat;
BITMAP bmp;
BOOL HBitmapToMat(HBITMAP& _hBmp, Mat& _mat)
{
	static uint8_t init_flag = 0;
	//BITMAP操作
	GetObject(_hBmp, sizeof(BITMAP), &bmp);
	int nChannels = bmp.bmBitsPixel == 1 ? 1 : bmp.bmBitsPixel / 8;
	int depth = bmp.bmBitsPixel == 1 ? IPL_DEPTH_1U : IPL_DEPTH_8U;
	//mat操作
	if (init_flag == 0)
	{
		v_mat.create(cvSize(bmp.bmWidth, bmp.bmHeight), CV_MAKETYPE(CV_8U, nChannels));
		init_flag = 1;
	}
	GetBitmapBits(_hBmp, bmp.bmHeight*bmp.bmWidth*nChannels, v_mat.data);
	_mat = v_mat;
	return TRUE;
}

void get_pc_screen(Mat &pc)
{
	Screen();
	HBitmapToMat(hBmp, pc);
	cvtColor(pc, pc, CV_BGRA2BGR);
}

void renju_vision_init()
{
	int i, j;
	for (i = 0; i < 13; i++){
		for (j = 0; j < 13; j++) {
			c_renju.board_HUMAN[i][j] = CHESS_NULL;
			c_renju.board_PC[i][j] = CHESS_NULL;
		}
	}
	c_renju.board_start_offset = Point(36, 102);
	c_renju.board_stop_offset = Point(325, 389);
	c_renju.M_piskvork_template=imread("E:/chess_template/ZJrenju/piskvork_template.png",1);
	c_renju.status = STATUS_END;
	c_renju.err_flag = 0;
	c_renju.run_flag = 0;
	c_renju.PC_busy = 0;
	c_renju.HUMAN_busy = 0;

	c_renju.clear_record();
}

void cut_mat(Mat &src, Mat &dst, Point Start, Point Stop) {
	if (src.cols == 0) return;
	if (Start.x > src.cols) return;
	if (Start.y > src.rows) return;
	if (Stop.x <= Start.x) return;
	if (Stop.y <= Start.y) return;
	dst = Mat(src, Rect(Start, Stop));
}

static void update_PC_screen() {//更新PC MAT
	Mat temp;
	get_pc_screen(temp);
	cut_mat(temp, c_renju.M_PC_raw, Point(0, 0), Point(534, 576));
}

void pixel_color_C3U8(Scalar *color, Mat &a, Point p) {//3通道像素值
	if (a.channels() != 3) {
		*color = Scalar(255, 255, 255);
		return;
	}
	if (p.x > a.cols || p.y > a.rows || p.x < 0 || p.y < 0) {
		*color = Scalar(255, 255, 255);
		return;
	}
	color->val[0] = a.at<Vec3b>(p.y, p.x)[0];
	color->val[1] = a.at<Vec3b>(p.y, p.x)[1];
	color->val[2] = a.at<Vec3b>(p.y, p.x)[2];
}

static e_chess piskvork_piece_read(int position_x,int position_y) {
	Point p;
	double average;
	Scalar color;

	average = 0;
	p = c_renju.piskvork_base + c_renju.board_start_offset;
	p.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0*position_x);
	p.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0*position_y);
	p.x += 4;
	p.y += 4;
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	average += color.val[0] + color.val[1] + color.val[2];

	p = c_renju.piskvork_base + c_renju.board_start_offset;
	p.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0*position_x);
	p.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0*position_y);
	p.x += 4;
	p.y -= 4;
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	average += color.val[0] + color.val[1] + color.val[2];

	p = c_renju.piskvork_base + c_renju.board_start_offset;
	p.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0*position_x);
	p.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0*position_y);
	p.x -= 4;
	p.y += 4;
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	average += color.val[0] + color.val[1] + color.val[2];

	p = c_renju.piskvork_base + c_renju.board_start_offset;
	p.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0*position_x);
	p.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0*position_y);
	p.x -= 4;
	p.y -= 4;
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	average += color.val[0] + color.val[1] + color.val[2];

	average /= 4.0;


	if (average < 200) return CHESS_BLACK;
	if (average > 600) return CHESS_WHITE;
	return CHESS_NULL;
}
static uint8_t piskvork_black_point(Point offset) {//寻找piskvork窗口这个点及四邻域有无黑点
	Scalar color;
	Point p;
	p = c_renju.piskvork_base + offset ;
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	if (color.val[0] < 20 && color.val[1] < 20 && color.val[2] < 20) return 1;
	p = c_renju.piskvork_base + offset + Point(0, 1);
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	if (color.val[0] < 20 && color.val[1] < 20 && color.val[2] < 20) return 1;
	p = c_renju.piskvork_base + offset + Point(0, -1);
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	if (color.val[0] < 20 && color.val[1] < 20 && color.val[2] < 20) return 1;
	p = c_renju.piskvork_base + offset + Point(1, 0);
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	if (color.val[0] < 20 && color.val[1] < 20 && color.val[2] < 20) return 1;
	p = c_renju.piskvork_base + offset+ Point(-1, 0);
	pixel_color_C3U8(&color, c_renju.M_PC_raw, p);
	if (color.val[0] < 20 && color.val[1] < 20 && color.val[2] < 20) return 1;
	return 0;
}
static void PC_board_read() {//读取piskvork的所有信息
	int i, j;
	double min, max;
	Point pmin, pmax;
	Mat temp_pc, temp_template;
	Mat M_match;
	cvtColor(c_renju.M_PC_raw, temp_pc, CV_BGR2GRAY);
	cvtColor(c_renju.M_piskvork_template, temp_template, CV_BGR2GRAY);
	matchTemplate(temp_pc, temp_template, M_match, TM_CCOEFF_NORMED);
	minMaxLoc(M_match, &min, &max, &pmin, &pmax);
	if (max > 0.6&&(pmax.x-1)<80&&(pmax.y-48)<80) {
		c_renju.window_find = 1;
		c_renju.piskvork_base = Point(pmax.x- 1,pmax.y- 48);
	}
	else {
		c_renju.window_find = 0;
	}
	if (c_renju.window_find == 1) {
		if (piskvork_black_point(Point(326, 435))) c_renju.black_first = 1;
		else c_renju.black_first = 0;

		if (piskvork_black_point(Point(351, 415))) c_renju.black_AI = 1;
		else c_renju.black_AI = 0;

		if (piskvork_black_point(Point(131, 414))) c_renju.white_AI = 1;
		else c_renju.white_AI = 0;
		
		for (i = 0; i < 13; i++) {
			for (j = 0; j < 13; j++) {
				c_renju.board_PC[i][j]=piskvork_piece_read(i, j);
			}
		}
	}
}

void PC_update_frame() {
	int i, j;
	update_PC_screen();
	if (c_renju.run_flag&&c_renju.M_PC_raw.cols > 0) {
		PC_board_read();
		c_renju.M_display = c_renju.M_PC_raw.clone();
		if (c_renju.window_find) {
			for (i = 0; i < 13; i++) {
				for (j = 0; j < 13; j++) {
					Point p_temp;
					p_temp = c_renju.piskvork_base + c_renju.board_start_offset;
					p_temp.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0f*i);
					p_temp.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0f*j);
					if (c_renju.board_PC[i][j] == CHESS_NULL) circle(c_renju.M_display, p_temp, 1, Scalar(0, 255, 0), 6, 8, 0);
					if (c_renju.board_PC[i][j] == CHESS_BLACK) circle(c_renju.M_display, p_temp, 1, Scalar(255, 0, 0), 6, 8, 0);
					if (c_renju.board_PC[i][j] == CHESS_WHITE) circle(c_renju.M_display, p_temp, 1, Scalar(0, 0, 255), 6, 8, 0);
				}
			}
		}
		c_renju.display_update = 1;
	}
	Sleep(50);
}
static void piskvork_click(Point offset,char type) {
	Point click_position_CV;
	POINT click_position_PC;
	POINT resume_position;

	if (c_renju.window_find == 0) return;

	click_position_CV = c_renju.piskvork_base + offset;
	click_position_PC.x = click_position_CV.x;
	click_position_PC.y = click_position_CV.y;

	GetCursorPos(&resume_position);
	SetCursorPos(click_position_PC.x, click_position_PC.y);
	if (type == 'L'||type=='l') {
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
	}
	else if (type == 'R' || type == 'r') {
		mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
	}
	SetCursorPos(resume_position.x, resume_position.y);
	Sleep(20);
}
static void piskvork_move(Point p) {
	Point offset;
	offset = c_renju.board_start_offset;
	offset.x += (int)((c_renju.board_stop_offset.x - c_renju.board_start_offset.x) / 12.0f*p.x);
	offset.y += (int)((c_renju.board_stop_offset.y - c_renju.board_start_offset.y) / 12.0f*p.y);
	piskvork_click(offset, 'L');
}
void board_control() {
	int i, j;
	int different_counter;
	Point p;
	e_status last_status;
	if (c_renju.window_find) {
		if (c_renju.status == STATUS_END) {
			c_renju.PC_busy = 1;
			while (c_renju.PC_busy) {
				PC_update_frame();
				if(c_renju.black_AI) piskvork_click(Point(308, 416), 'R');//终止状态黑色AI必须关闭
				else if(c_renju.white_AI) piskvork_click(Point(83 , 414), 'R');//终止状态白色AI必须关闭
				else if(c_renju.board_PC_empty()==0) piskvork_click(Point(10, 57), 'L');//清空棋盘
				else if(c_renju.black_first==0) piskvork_click(Point(10, 57),'L');//必须黑先
				else c_renju.PC_busy = 0;
				Sleep(50);
				PC_update_frame();
				if(c_renju.PC_busy) cout << "loop at board_control->STATUS_END" << endl;
			}
		}
		if (c_renju.status == STATUS_PLAYING) {
			c_renju.PC_busy = 1;
			while (c_renju.PC_busy) {//检查AI开关
				PC_update_frame();
				if (c_renju.PC_chess_color == CHESS_WHITE&&c_renju.white_AI == 0) piskvork_click(Point(83, 414), 'R');//打开白色AI
				else if (c_renju.PC_chess_color == CHESS_BLACK&&c_renju.white_AI == 1) piskvork_click(Point(83, 414), 'R');//关闭白色AI
				else if (c_renju.PC_chess_color == CHESS_BLACK&&c_renju.black_AI == 0) piskvork_click(Point(308, 416), 'R');//打开黑色AI
				else if (c_renju.PC_chess_color == CHESS_WHITE&&c_renju.black_AI == 1) piskvork_click(Point(308, 416), 'R');//关闭黑色AI
				else c_renju.PC_busy = 0;
				Sleep(50);
				PC_update_frame();
				if(c_renju.PC_busy) cout<< "loop at board_control->STATUS_PLAYING" << endl;
			}

			if (c_renju.whose_move == c_renju.PC_chess_color){//电脑行棋
				different_counter = c_renju.cmp_board(c_renju.board_PC, c_renju.board_record[c_renju.current_move_num - 1],&p);
				if (different_counter == 1) {
					c_renju.copy_board(c_renju.board_PC, c_renju.board_record[c_renju.current_move_num++]);
					//这里添加传送给机器人下棋
				}
				else if (different_counter == 0);//没有动作
				else {//出错
					c_renju.err_flag = 1;
					while (1) {
						cout << "error at board_control->STATUS_PLAYING->PC's move->different_counter>1" << endl;
					}
				}
			}
			else {//人类行棋

			}
		}
	}
	else {
		while (c_renju.window_find == 0) {//等待找到窗口
			cout << "loop at board_control->piskvork window not found" << endl;
			PC_update_frame();
			Sleep(50);
		}
		cout << "board-control->piskvork window not found->refound, wait for 1sec" << endl;
		Sleep(1000);
		cout << "board-control->piskvork window not found->refound, 1sec achived" << endl;
	}

	last_status = c_renju.status;
}

void mouse_handler(int event, int x, int y, int flags, void* param) {
	Mat *a;
	a = (Mat*)param;
	Scalar color;
	switch (event) {
	case EVENT_MOUSEMOVE:
		break;
	case EVENT_LBUTTONDOWN:
		printf("x:%d\ty:%d\n", x-c_renju.piskvork_base.x, y - c_renju.piskvork_base.y);
		pixel_color_C3U8(&color,*a, Point(x, y));
		cout << "0:"<<color.val[0]<<"\t1:"<< color.val[1]<<"\t2:"<<color.val[2] << endl;
		break;
	case EVENT_LBUTTONUP:
		break;
	default:
		break;
	}
}

void renju_vision_task_interface() {//显示、鼠标进程
	cout << "interface_task_start" << endl;
	Sleep(100);
	namedWindow("PC", CV_WINDOW_AUTOSIZE);
	cout << "press s to start process" << endl;
	while (waitKey(10) != 's');
	c_renju.run_flag = 1;
	setMouseCallback("PC", mouse_handler, &c_renju.M_PC_raw);
	while (1) {
		if (c_renju.run_flag&&c_renju.display_update&&c_renju.M_display.cols>0) {
			c_renju.display_update = 0;
			imshow("PC", c_renju.M_display);
		}
		waitKey(10);
	}
}

void renju_vision_task_cv(){//机器视觉进程
	int i, j;
	Sleep(50);
	cout << "cv_task_start" << endl;
	while (1) {
		PC_update_frame();//读取PC棋盘
		if (c_renju.run_flag&&c_renju.M_PC_raw.cols > 0) {
			//读取实体棋盘

			board_control();//操控电脑和实体棋盘


			
		}
		Sleep(50);
	}
}

void renju_vision_tasks_create(){//创建进程
	renju_vision_init();


	thread thread_interface(renju_vision_task_interface);
	thread thread_cv(renju_vision_task_cv);

	thread_interface.join();
	thread_cv.join();
	
}