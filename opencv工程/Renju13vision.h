#pragma once

using namespace cv;

typedef enum
{
	CHESS_NULL,
	CHESS_BLACK,
	CHESS_WHITE
}e_chess;

typedef enum
{
	STATUS_WAIT,
	STATUS_PLAYING,
	STATUS_END
}e_status;

class c_renju_t{
public:
	uint8_t run_flag;//为1时程序才运行
	uint8_t err_flag;//遇到错误

	e_chess board_record[500][13][13];//记录每一步的棋盘
	uint16_t current_move_num;//当前手数

	uint8_t display_update;
	e_status status;
	e_chess board_PC[13][13];
	e_chess board_HUMAN[13][13];
	Mat M_display;
	Mat M_PC_raw;
	Mat M_piskvork_template;
	uint8_t window_find;//找到piskvork窗口
	Point piskvork_base;//piskvork窗口坐标
	Point board_start_offset;//棋盘左上角偏移
	Point board_stop_offset;//棋盘右下角偏移

	e_chess PC_chess_color;//PC执棋颜色
	e_chess HUMAN_chess_color;//人类执棋颜色
	e_chess whose_move;//当前谁在思考

	uint8_t PC_busy;//正在按键操作等情况
	uint8_t HUMAN_busy;//正在摆放棋子等情况



	uint8_t white_AI;
	uint8_t black_AI;
	uint8_t black_first;

	void clear_record() {
		int i, j, k;
		current_move_num = 1;
		for (i = 0; i < 500; i++) {
			for (j = 0; j < 13; j++) {
				for (k = 0; k < 13; k++) {
					board_record[i][j][k] = CHESS_NULL;
				}
			}
		}
	}
	void copy_board(e_chess src[13][13], e_chess dst[13][13]) {
		int i, j;
		for (i = 0; i < 13; i++) {
			for (j = 0; j < 13; j++) {
				dst[i][j] = src[i][j];
			}
		}
	}
	int cmp_board(e_chess A[13][13], e_chess B[13][13],Point *p) {//返回不一样棋子的个数，p里存放最后以及比较结果为不一样的坐标
		int i, j;
		int different_counter;
		different_counter = 0;
		for (i = 0; i < 13; i++) {
			for (j = 0; j < 13; j++) {
				if (A[i][j] != B[i][j]) {
					different_counter++;
					*p = Point(i, j);
				}
			}
		}
		return different_counter;
	}
	uint8_t board_PC_empty() {
		int i, j;
		for (i = 0; i < 13; i++){
			for (j = 0; j < 13; j++) {
				if (board_PC[i][j] != CHESS_NULL) return 0;
			}
		}

		return 1;
	}
	uint8_t board_HUMAN_empty() {
		int i, j;
		for (i = 0; i < 13; i++) {
			for (j = 0; j < 13; j++) {
				if (board_HUMAN[i][j] != CHESS_NULL) return 0;
			}
		}
		return 1;

	}

};
extern c_renju_t c_renju;


void renju_vision_tasks_create();

